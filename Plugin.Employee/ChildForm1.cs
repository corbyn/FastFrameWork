﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;

namespace Plugin.Employee
{
    public partial class ChildForm1 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public ChildForm1()
        {
            InitializeComponent();
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraMessageBox.Show(string.Format("窗体[{0}]的<{1}>按钮事件触发.", this.Name, "系统设置"));
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraMessageBox.Show(string.Format("窗体[{0}]的<{1}>按钮事件触发.", this.Name, "用户令牌"));
        }

        private void ChildForm1_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = Business.GetAllGrade();
        }
    }
}