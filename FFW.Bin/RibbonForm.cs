﻿// FastFrameWork 快速开发框架
// 创建时间：2015-06-23 22:56:33 
// 创建作者：穆建情
// 联系方式：66767376@QQ.Com
// 技术博客:http://www.cnblogs.com/MuNet/category/856588.html
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using System.Xml;
using System.IO;
using DevExpress.XtraTabbedMdi;
using DevExpress.XtraEditors;
using System.Configuration;
using System.Threading;
using System.Runtime.InteropServices;
using Spring.Context;
using Spring.Context.Support;

namespace FFW.Bin
{
    public partial class RibbonForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        public RibbonForm()
        {
            InitializeComponent();
            this.ribbon.ShowCustomizationMenu += (s, e) => { e.ShowCustomizationMenu = false; };
        }

        private void RibbonForm_Load(object sender, EventArgs e)
        {
            this.LoadPlugin();
        }

        private void LoadPlugin()
        {
            ribbon.ForceInitialize();
            IApplicationContext ctx = ContextRegistry.GetContext();
            var Objects = ctx.GetObjectDefinitionNames();
            foreach (var item in Objects)
            {
                var frm = ctx.GetObject(item) as DevExpress.XtraBars.Ribbon.RibbonForm;
                frm.TopLevel = false;
                frm.TopMost = false;
                frm.MdiParent = this;
                frm.ShowInTaskbar = false;
                frm.FormBorderStyle = FormBorderStyle.None;
                frm.WindowState = FormWindowState.Maximized;
                frm.Dock = DockStyle.Fill;
                frm.Text = "";
                foreach (RibbonPage page in frm.Ribbon.Pages)
                    ribbon.Pages.Add(new RibbonPage { Text = page.Text, Tag = frm.GetType().FullName });
            }
        }

        #region Ribbon_事件

        private void ribbon_Merge(object sender, DevExpress.XtraBars.Ribbon.RibbonMergeEventArgs e)
        {
            RibbonControl parentRRibbon = sender as RibbonControl;
            RibbonControl childRibbon = e.MergedChild;
            parentRRibbon.StatusBar.MergeStatusBar(childRibbon.StatusBar);
        }

        private void ribbon_UnMerge(object sender, RibbonMergeEventArgs e)
        {
            RibbonControl parentRRibbon = sender as RibbonControl;
            parentRRibbon.StatusBar.UnMergeStatusBar();
        }

        private void ribbon_SelectedPageChanging(object sender, RibbonPageChangingEventArgs e)
        {
            var frm = this.MdiChildren.FirstOrDefault(x => x.GetType().FullName.Equals(e.Page.Tag));
            frm?.Show();
            frm?.Activate();
            frm?.BringToFront();
        }

        #endregion      



    }
}