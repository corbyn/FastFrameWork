﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace FFW.Utilitys
{
    /// <summary>
    /// 图形处理类
    /// </summary>
    public class ImageHelper
    {
        /// <summary>
        /// 按比例缩小图片，自动计算宽度
        /// </summary>
        /// <param name="oldPic">源图像</param>
        /// <param name="height">缩小至高度</param>
        public static Bitmap SmallPicWidth(Image oldPic, double height)
        {            
            System.Drawing.Bitmap objNewPic;
            try
            {
                int intWidth = Convert .ToInt32((height / oldPic.Height) * oldPic.Width);
                int intHeight = Convert.ToInt32(height);
                objNewPic = new System.Drawing.Bitmap(oldPic, intWidth, intHeight);
                return objNewPic;
            }
            catch (Exception exp) { throw exp; }
            finally
            {
                oldPic = null;
                objNewPic = null;
            }
        }

        /// <summary>
        /// 按指定高度与宽度，缩小图片
        /// </summary>
        /// <param name="oldPic">源图像</param>
        /// <param name="intWidth">缩小至宽度</param>
        /// <param name="intHeight">缩小至高度</param>
        public static Bitmap SmallPic(Bitmap oldPic, int intWidth, int intHeight)
        {
            System.Drawing.Bitmap objNewPic;
            try
            {
                objNewPic = new System.Drawing.Bitmap(oldPic, intWidth, intHeight);
                return objNewPic;
            }
            catch (Exception exp)
            { throw exp; }
            finally
            {
                oldPic = null;
                objNewPic = null;
            }
        }

        /// <summary>
        /// 按指定高度缩小图片
        /// </summary>
        /// <param name="originBmp">源图像</param>
        /// <param name="height">缩小至高度</param>
        public static Bitmap PicSized(Bitmap originBmp, double height)
        {
            int intWidth = Convert.ToInt32((height / originBmp.Height) * originBmp.Width);
            int intHeight=Convert.ToInt32(height);
            Bitmap resizedBmp = new Bitmap(intWidth, intHeight);
            Graphics g = Graphics.FromImage(resizedBmp);
            //设置高质量插值法  
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
            //设置高质量,低速度呈现平滑程度  
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            //消除锯齿
            g.DrawImage(originBmp, new Rectangle(0, 0, intWidth, intHeight), new Rectangle(0, 0, originBmp.Width, originBmp.Height), GraphicsUnit.Pixel);
            g.Dispose();
            return resizedBmp;
        }

        /// <summary>
        /// 将byte[]转换为Image
        /// </summary>
        /// <param name="bytes">字节数组</param>
        /// <returns>Image</returns>
        public static Image ReadImage(byte[] bytes)
        {
            if (bytes == null)
                return null;
            MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
            BinaryFormatter bf = new BinaryFormatter();
            object obj = bf.Deserialize(ms);
            ms.Close();
            return (Image)obj;
        }
        /// <summary>
        /// 将Image转换为byte[]
        /// </summary>
        /// <param name="image">Image</param>
        /// <returns>byte[]</returns>
        public static byte[] ConvertImage(Image image)
        {
            if (image == null)
                return null;

            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, (object)image);
                ms.Close();
                return ms.ToArray();
            }
        }

        /// <summary>
        /// 图像转成Base64字符
        /// </summary>
        /// <param name="image"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format=null)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                try
                {

                    if (format == null)
                        format = System.Drawing.Imaging.ImageFormat.Png;
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();
                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
                catch { return ""; }
            }
        }
        /// <summary>
        /// Base64字符转成图像
        /// </summary>
        /// <param name="base64String"></param>
        /// <returns></returns>
        public static Image Base64ToImage(string base64String)
        {
            try
            {
                // Convert Base64 String to byte[]
                byte[] imageBytes = Convert.FromBase64String(base64String);
                using (MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    // Convert byte[] to Image
                    ms.Write(imageBytes, 0, imageBytes.Length);
                    Image image = Image.FromStream(ms, false,true);
                    return image;
                }
            }
            catch { return null; }
        }
    }
}
