﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using System.IO;
using FFW.Entity;

namespace Plugin.Dept
{
    //Agile.DataAccess 数据库访问类库具体使用方法请参考 http://www.cnblogs.com/MuNet/p/5740833.html
    public partial class ChildForm2 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public ChildForm2()
        {
            InitializeComponent();
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraMessageBox.Show(string.Format("窗体[{0}]的<{1}>按钮事件触发.", this.Name, "系统调试"));
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraMessageBox.Show(string.Format("窗体[{0}]的<{1}>按钮事件触发.", this.Name, "部门设置"));
        }

        private void btn_Design_ItemClick(object sender, ItemClickEventArgs e)
        {
            var report = this.Prepare();
            report.Design();
        }

        private void btn_Preview_ItemClick(object sender, ItemClickEventArgs e)
        {
            var report = this.Prepare();
            report.Preview();
        }

        private void btn_Print_ItemClick(object sender, ItemClickEventArgs e)
        {
            var report = this.Prepare();
            report.Print();
        }

        private FFW.Report.ReportEx Prepare()
        {
            var report = new FFW.Report.ReportEx();
            report.DataSources.Add("",Business.GetAllGrade());
            report.DataSources.Add("Student",Business.GetAllStudent());
            report.Parameters.Add("参数1", "FastFrameWork 快速开发框架");
            report.Parameters.Add("参数2", DateTime.Now);
            report.LoadFrom(Path.Combine(Application.StartupPath, "Report", "test.frx"));
            return report;
        }

        private void ChildForm2_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = Business.GetAllStudent();
        }

    }
}