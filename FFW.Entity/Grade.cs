﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FFW.Entity
{
   public  class Grade
    {
       public int ID { get; set; }
       public string Class { get; set; }
       public int Level { get; set; }
    }
}
